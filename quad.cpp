#include "quad.hpp"
#include <sstream>

using namespace std;

Quad::Quad(Vec3d position, Vec3d normal, unsigned int internalColor){
	
	this->internalColor = internalColor;
	square = Square(position, normal);
	
	if(normal[0] == -1){
		
		vertices[0] = position[0];
		vertices[1] = position[1];
		vertices[2] = position[2];
		
		vertices[3] = position[0];
		vertices[4] = position[1];
		vertices[5] = position[2] + 1;
		
		vertices[6] = position[0];
		vertices[7] = position[1] + 1;
		vertices[8] = position[2] + 1;
		
		vertices[9] = position[0];
		vertices[10] = position[1] + 1;
		vertices[11] = position[2];
		
		
	}
	else if(normal[0] == +1){
		
		vertices[0] = position[0];
		vertices[1] = position[1];
		vertices[2] = position[2];
		
		vertices[3] = position[0];
		vertices[4] = position[1] + 1;
		vertices[5] = position[2];
		
		vertices[6] = position[0];
		vertices[7] = position[1] + 1;
		vertices[8] = position[2] + 1;
		
		vertices[9] = position[0];
		vertices[10] = position[1];
		vertices[11] = position[2] + 1;
	
	}
	else if(normal[1] == -1){
		
		vertices[0] = position[0];
		vertices[1] = position[1];
		vertices[2] = position[2];
		
		vertices[3] = position[0] + 1;
		vertices[4] = position[1];
		vertices[5] = position[2];
		
		vertices[6] = position[0] + 1;
		vertices[7] = position[1];
		vertices[8] = position[2] + 1;
		
		vertices[9] = position[0];
		vertices[10] = position[1];
		vertices[11] = position[2] + 1;
		
	}
	else if(normal[1] == +1){
		
		vertices[0] = position[0];
		vertices[1] = position[1];
		vertices[2] = position[2];
		
		vertices[3] = position[0];
		vertices[4] = position[1];
		vertices[5] = position[2] + 1;
		
		vertices[6] = position[0] + 1;
		vertices[7] = position[1];
		vertices[8] = position[2] + 1;
		
		vertices[9] = position[0] + 1;
		vertices[10] = position[1];
		vertices[11] = position[2];
		
	}
	else if(normal[2] == -1){
		
		vertices[0] = position[0];
		vertices[1] = position[1];
		vertices[2] = position[2];
		
		vertices[3] = position[0];
		vertices[4] = position[1] + 1;
		vertices[5] = position[2];
		
		vertices[6] = position[0] + 1;
		vertices[7] = position[1] + 1;
		vertices[8] = position[2];
		
		vertices[9] = position[0] + 1;
		vertices[10] = position[1];
		vertices[11] = position[2];
		
	}
	else if(normal[2] == +1){
		
		vertices[0] = position[0];
		vertices[1] = position[1];
		vertices[2] = position[2];
		
		vertices[3] = position[0] + 1;
		vertices[4] = position[1];
		vertices[5] = position[2];
		
		vertices[6] = position[0] + 1;
		vertices[7] = position[1] + 1;
		vertices[8] = position[2];
		
		vertices[9] = position[0];
		vertices[10] = position[1] + 1;
		vertices[11] = position[2];
		
	}
	
}

unsigned int Quad::getInternalColor(){
	return internalColor;
}

void Quad::setInternalColor(unsigned int internalColor){
	this->internalColor = internalColor;
}

Square Quad::getSquare()const{
	return square;
}

float& Quad::operator[](int i){
	return vertices[i];
}

const float& Quad::operator[](int i)const{
	return vertices[i];
}

string Quad::toString()const{
	
	stringstream ss;
	
	ss << "Quad:";
	ss << "\t" << "( " << vertices[0] << ", " << vertices[1] << ", " << vertices[2] << " )" << endl;
	ss << "\t" << "( " << vertices[3] << ", " << vertices[4] << ", " << vertices[5] << " )" << endl;
	ss << "\t" << "( " << vertices[6] << ", " << vertices[7] << ", " << vertices[8] << " )" << endl;
	ss << "\t" << "( " << vertices[9] << ", " << vertices[10] << ", " << vertices[11] << " )" << endl;
	
	return ss.str();
	
}
