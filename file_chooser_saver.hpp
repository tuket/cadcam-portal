#include <gtkmm.h>

class FileChooserSaver : public Gtk::FileChooserDialog {
	
public:

    static std::string getFileName();
protected:
    static Gtk::Main kit;
    std::string chosenFile;

    FileChooserSaver(const Glib::ustring& title, Gtk::FileChooserAction action = Gtk::FILE_CHOOSER_ACTION_SAVE) :
    Gtk::FileChooserDialog(title, action) {
        chosenFile = std::string("");
        add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
        add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);
        signal_response().connect(sigc::mem_fun(*this,
                &FileChooserSaver::on_my_response));
    }

    void on_my_response(int response_id);
    
};

