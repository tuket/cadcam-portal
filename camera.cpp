#include "camera.hpp"
#include <cmath>

Camera::Camera(){
	_near = 0.1;
	_far = 10000;
	heading = 0;
	pitch = 0;
	pos = Vec3f(0, 0, 0);
}

Camera::Camera(const Vec3f& pos){
	_near = 0.1;
	_far = 10000;
	heading = 0;
	pitch = 0;
	this->pos = pos;
}

Camera::Camera(const Vec3f& pos, float heading, float pitch){
	
	_near = 0.1;
	_far = 10000;
	this->heading = heading;
	this->pitch = pitch;
	this->pos = pos;
	
}

Vec3f Camera::getPosition()const{
	return pos;
}

void Camera::setPosition(const Vec3f& pos){
	this->pos = pos;
}

float Camera::getHeading()const{
	return heading;
}

float Camera::getPitch()const{
	return pitch;
}

void Camera::setHeading(float a){
	while(a>360) a-= 360;
	while(a<360) a+= 360;
	heading = a;
}

void Camera::setPitch(float a){
	while(a>360) a-= 360;
	while(a<360) a+= 360;
	pitch = a;
}

float Camera::getNear()const{
	return _near;
}

float Camera::getFar()const{
	return _far;
}

void Camera::setNear(float _near){
	this->_near = _near;
}

void Camera::setFar(float _far){
	this->_far = _far;
}

Vec3f Camera::getDirection()const{
	
	Vec3f res(0, 0, -1);
	res[0] = -sin( heading*M_PI/180.f ) * cos( pitch*M_PI/180.f );
	res[1] = sin( pitch*M_PI/180.f );
	res[2] = -cos( heading*M_PI/180.f ) * cos( pitch*M_PI/180.f );
	return res;
	
}
