#include "math/vec3d.hpp"
#include <string>

#ifndef SQUARE
#define SQUARE

class Square{
	
	Vec3d position;
	Vec3d normal;
	bool valid;
	
public:

	Square(){ valid = false; }
	Square(const Vec3d& position, const Vec3d& normal);
	
	Vec3d getPosition()const;
	Vec3d getNormal()const;
	
	void setPosition(const Vec3d& position);
	void setNormal(const Vec3d& normal);
	
	bool getValid();
	void setValid(bool valid);
	
	bool operator==(const Square& s)const;
	
	std::string toString();
	
};

#endif
