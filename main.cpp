#include <GL/freeglut.h>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <FreeImage.h>
#include <vector>
#include "camera.hpp"
#include "cube_of_bits.hpp"
#include "quad.hpp"
#include "square.hpp"
#include "color3f.hpp"
#include "file_chooser.hpp"
#include "file_chooser_saver.hpp"

using namespace std;

void init();
void display();
void timer(int t);
void reshape(int w, int h);
void onMouseClick(int button, int state, int x, int y);
void onDrag(int x, int y);
void onKeyDown(unsigned char key, int x, int y);
void onKeyUp(unsigned char key, int x, int y);
void loadImageFile(const char* nombre);
void select(int x, int y);

GLuint tileTexture;
Camera camera;
const int fps = 50;
const float fovy = 100;
float speed = 0.0f;
bool rightMouse = false;
int mouseX, mouseY;
bool wDown = false, aDown = false, sDown = false, dDown = false;

CubeOfBits cob;
vector<Quad> quads;
Square selected;

int main(int argc, char** argv){
	
	gtk_init (&argc, &argv);
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(600, 600);
	glutInitWindowPosition(64, 64);
	glutCreateWindow("CAD CAM");
	
	init();
	
	glutMainLoop();
	
	return 0;
	
}

void init(){
	
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDisable(GL_CULL_FACE);
	glDepthFunc(GL_LEQUAL);
	
	glClearDepth(1.0);
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	
	// load texture
	glGenTextures(1, &tileTexture);
	glBindTexture(GL_TEXTURE_2D, tileTexture);
	loadImageFile("tile.png");
	glEnable(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	
	// init cube of bits
	cob = CubeOfBits();
	quads = cob.getVisualRepresentation();
	selected.setPosition(Vec3d(0, 0, 0));
	selected.setNormal(Vec3d(1, 0, 0));
	selected.setValid(true);
	
	// camera at the center
	camera =
		Camera(
			Vec3f(
				(cob.getLeft()+cob.getRight())/2,
				(cob.getBot()+cob.getTop())/2,
				(cob.getFront()+cob.getBack())/2
			),
			0, 0
		);
	
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutTimerFunc(1000/fps, timer, glutGet(GLUT_ELAPSED_TIME));
	glutMouseFunc(onMouseClick);
	glutMotionFunc(onDrag);
	glutKeyboardFunc(onKeyDown);
	glutKeyboardUpFunc(onKeyUp);
	
}

void reshape(int w, int h){
	
	glViewport(0, 0, w, h);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(fovy, w/((float)h), camera.getNear(), camera.getFar());
	
}

void display(){
	
	glClear(GL_COLOR_BUFFER_BIT);
	glClear(GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
	
	glPushMatrix();
	
		// Camera transform
		glRotatef(-camera.getPitch(), 1, 0, 0);
		glRotatef(-camera.getHeading(), 0 , 1, 0);
		glTranslatef(-camera.getPosition()[0], -camera.getPosition()[1], -camera.getPosition()[2]);
		
		// Draw quads
		for(int i=0; i<quads.size(); i++){
			if( selected.getValid() && selected == quads[i].getSquare() ){
				glColor3f(1, 0.5, 0.5);
			}
			else{
				glColor3f(1, 1, 1);
			}
			glBegin(GL_QUADS);
				glTexCoord2f(0, 0);
				glVertex3f(quads[i][0], quads[i][1], quads[i][2]);
				glTexCoord2f(0, 1);
				glVertex3f(quads[i][3], quads[i][4], quads[i][5]);
				glTexCoord2f(1, 1);
				glVertex3f(quads[i][6], quads[i][7], quads[i][8]);
				glTexCoord2f(1, 0);
				glVertex3f(quads[i][9], quads[i][10], quads[i][11]);
			glEnd();
		}
		
	glPopMatrix();
	
	glutSwapBuffers();
	
}

void timer(int t){
	
	
	int elapsed = 1000/fps;
	
	// Update position
	if(wDown){
		speed = 1.f;
	}
	else{
		
		speed = 0.0f;
		if(sDown){
		speed = -1.f;
		}
		else{
			speed = 0.0f;
		}
		
	}
	
	// update camera position
	Vec3f newPosition = camera.getPosition();
	Vec3f displacement = camera.getDirection() * (speed * elapsed / 1000.0);
	newPosition += displacement;
	camera.setPosition(newPosition);
	
	glutTimerFunc(elapsed, timer, 0);
	glutPostRedisplay();
	
}

void onMouseClick(int button, int state, int x, int y){
	
	// left click -> select quad
	if(button == GLUT_LEFT_BUTTON){
		
		select(x, y);
		
	}
	
	// right click -> orient the camera
	else if(button == GLUT_RIGHT_BUTTON){
		
		rightMouse = (state == GLUT_DOWN)? true : false;
		mouseX = x;
		mouseY = y;
		
	}
	
}

void onDrag(int x, int y){
	
	// orient the camera
	if(rightMouse){
		
		float heading = camera.getHeading();
		float pitch = camera.getPitch();
		const float sensitivity = 0.25f;
		
		heading += (mouseX-x)*sensitivity;
		pitch += (mouseY-y)*sensitivity;
		
		camera.setHeading(heading);
		camera.setPitch(pitch);
		
		mouseX = x;
		mouseY = y;
		
	}
	
}

void onKeyDown(unsigned char key, int x, int y){
	
	switch(key){
		
		case 'w':
			wDown = true;
		break;
		
		case 'a':
			aDown = true;
		break;
		
		case 's':
			sDown = true;
		break;
		
		case 'd':
			dDown = true;
		break;
		
	};
	
	if( key == '1' ){	// push
		
		if(selected.getValid()){
			
			selected = cob.push(selected);
			quads = cob.getVisualRepresentation();
			
		}
		
	}
	else if( key == '2' ){	// pop
		
		if(selected.getValid()){
			
			selected = cob.pull(selected);
			quads = cob.getVisualRepresentation();
			
		}
		
	}
	else if( key == 'p' ){	// place the camera at the center
		
		camera.setPosition
		(
			Vec3f(
				(cob.getLeft()+cob.getRight())/2,
				(cob.getBot()+cob.getTop())/2,
				(cob.getFront()+cob.getBack())/2
			)
		);
		camera.setHeading(0);
		camera.setPitch(0);
		
	}
	else if( key == 'k' ){	// save
		
		string fileName = FileChooserSaver::getFileName();
		
		if(fileName != ""){
			cob.save("1.map");
		}
		
	}
	else if( key == 'l' ){	// load
		
		string fileName = FileChooser::getFileName();
		
		if(fileName != ""){
		
			cob = CubeOfBits(fileName);
			quads = cob.getVisualRepresentation();
			selected.setValid(false);
			camera.setPosition
			(
				Vec3f(
					(cob.getLeft()+cob.getRight())/2,
					(cob.getBot()+cob.getTop())/2,
					(cob.getFront()+cob.getBack())/2
				)
			);
			camera.setHeading(0);
			camera.setPitch(0);
		}
	
	}
	
}

void onKeyUp(unsigned char key, int x, int y){
	
	switch(key){
		
		case 'w':
			wDown = false;
		break;
		
		case 'a':
			aDown = false;
		break;
		
		case 's':
			sDown = false;
		break;
		
		case 'd':
			dDown = false;
		break;
		
	};
	
}

void loadImageFile(const char* nombre)
{
	// Deteccion del formato, lectura y conversion a BGRA
	FREE_IMAGE_FORMAT formato = FreeImage_GetFileType(nombre,0);
	FIBITMAP* imagen = FreeImage_Load(formato, nombre); 
	if(imagen==NULL) cerr << "Fallo carga de imagen " << nombre <<endl;
	FIBITMAP* imagen32b = FreeImage_ConvertTo32Bits(imagen);

	// Lectura de dimensiones y colores
	int w = FreeImage_GetWidth(imagen32b);
	int h = FreeImage_GetHeight(imagen32b);
	GLubyte* texeles = FreeImage_GetBits(imagen32b);

	// Carga como textura actual
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, texeles);

	// Liberar recursos
	FreeImage_Unload(imagen);
	FreeImage_Unload(imagen32b);
}

// select the pixel at the mouse pointer
// then infere the square by the pixel color
void select(int x, int y){
	
	glDisable(GL_TEXTURE_2D);
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
	
	glPushMatrix();
	
		// Camera transform
		glRotatef(-camera.getPitch(), 1, 0, 0);
		glRotatef(-camera.getHeading(), 0 , 1, 0);
		glTranslatef(-camera.getPosition()[0], -camera.getPosition()[1], -camera.getPosition()[2]);
		
		// Draw quads
		for(int i=0; i<quads.size(); i++){
			Color3f col(quads[i].getInternalColor());
			glColor3f(col.getR(), col.getG(), col.getB());
			glBegin(GL_QUADS);
				glVertex3f(quads[i][0], quads[i][1], quads[i][2]);
				glVertex3f(quads[i][3], quads[i][4], quads[i][5]);
				glVertex3f(quads[i][6], quads[i][7], quads[i][8]);
				glVertex3f(quads[i][9], quads[i][10], quads[i][11]);
			glEnd();
		}
		
	glPopMatrix();
	
	GLint viewport[4];
	glGetIntegerv(  GL_VIEWPORT, viewport );
	GLint yviewport = viewport[3] - y;

	unsigned char r, g, b;
	glReadPixels( x, yviewport, 1, 1, GL_RED, GL_UNSIGNED_BYTE, &r );
	glReadPixels( x, yviewport, 1, 1, GL_GREEN, GL_UNSIGNED_BYTE, &g );
	glReadPixels( x, yviewport, 1, 1, GL_BLUE, GL_UNSIGNED_BYTE, &b );
	
	// c must be in 24bit format
	unsigned int c = r << 8;
	c |= g;
	c = c << 8;
	c |= b;
	
	// usefull print for debugging
	cout << "mouse: " << c << endl;
	cout << (int)r << "," << (int)g << "," << (int)b << endl;
	
	// find which quad has internal color c
	for(int i=0; i<quads.size(); i++){
		if(quads[i].getInternalColor() == c ){
			selected = quads[i].getSquare();
		}
		
	}
	
	glEnable(GL_TEXTURE_2D);
	
}
