
#ifndef COLOR3F
#define COLOR3f

class Color3f{
	
	float r, g, b;

public:
	
	Color3f();
	Color3f(float r, float g, float b);
	Color3f(unsigned int c);
	Color3f(unsigned int r, unsigned int g, unsigned int b);
	
	float getR()const;
	float getG()const;
	float getB()const;
	
	unsigned int getIntR()const;
	unsigned int getIntG()const;
	unsigned int getIntB()const;
	
	unsigned int getInt()const;
	
};

#endif
