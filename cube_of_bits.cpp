#include "cube_of_bits.hpp"

#include <iostream>
#include <sstream>
#include <fstream>

using namespace std;

CubeOfBits::CubeOfBits(){
	
	aleft = left = 0;
	abot = bot = 0;
	afront = front = 0;
	aright = right = 6;
	atop = top = 6;
	aback = back = 6;
	
	// reserve memory
	cube = new char**[aright-aleft];
	cube[0] = new char*[(atop-abot)*(aright-aleft)];
	cube[0][0] = new char[(atop-abot)*(aright-aleft)*(aback-afront)];
	
	// set everything to empty
	for(int i=0; i<(atop-abot)*(aright-aleft)*(aback-afront); i++){
		cube[0][0][i] = 0;
	}
	
	// make up the structure
	for(int i=0; i<(aright-aleft); i++){
		cube[i] = &cube[0][i*(atop-abot)];
		for(int j=0; j<(atop-abot); j++){
			cube[i][j] = &cube[0][0][ i*(atop-abot)*(aback-afront) + j*(aback-afront) ];
		}
	}
	
}

CubeOfBits::CubeOfBits(unsigned int width, unsigned int height, unsigned int depth){
	
	aleft = left = 0;
	abot = bot = 0;
	afront = front = 0;
	aright = right = width;
	atop = top = height;
	aback = back = depth;
	
	// reserve memory
	cube = new char**[aright-aleft];
	cube[0] = new char*[(atop-abot)*(aright-aleft)];
	cube[0][0] = new char[(atop-abot)*(aright-aleft)*(aback-afront)];
	
	// set everything to empty
	for(int i=0; i<(atop-abot)*(aright-aleft)*(aback-afront); i++){
		cube[0][0][i] = 0;
	}
	
	// make up the structure
	for(int i=0; i<(aright-aleft); i++){
		cube[i] = &cube[0][i*(atop-abot)];
		for(int j=0; j<(atop-abot); j++){
			cube[i][j] = &cube[0][0][ i*(atop-abot)*(aback-afront) + j*(aback-afront) ];
		}
	}
	
}

CubeOfBits::CubeOfBits(string fileName){
	
	ifstream fs;
	fs.open(fileName.c_str());
	
	left = bot = front = aleft = abot = afront = 0;
	fs >> aright;
	right = aright;
	fs >> atop;
	top = atop;
	fs >> aback;
	back = aback;
	
	// reserve memory
	cube = new char**[aright-aleft];
	cube[0] = new char*[(atop-abot)*(aright-aleft)];
	cube[0][0] = new char[(atop-abot)*(aright-aleft)*(aback-afront)];
	
	// make up the structure
	for(int i=0; i<(aright-aleft); i++){
		cube[i] = &cube[0][i*(atop-abot)];
		for(int j=0; j<(atop-abot); j++){
			cube[i][j] = &cube[0][0][ i*(atop-abot)*(aback-afront) + j*(aback-afront) ];
		}
	}
	
	// copy from file
	int a;
	for(int x=0; x<right; x++)
	for(int y=0; y<top; y++)
	for(int z=0; z<back; z++)
	{
		fs >> a;
		cube[x][y][z] = a;
	}
	
}

int CubeOfBits::getBot()const{ return bot; }
int CubeOfBits::getTop()const{ return top; }
int CubeOfBits::getBack()const{ return back; }
int CubeOfBits::getFront()const{ return front; }
int CubeOfBits::getLeft()const{ return left; }
int CubeOfBits::getRight()const{ return right; }

Square CubeOfBits::push(const Square& square){
	
	bool posi;	// positive push
	{
		int nz = 0;
		int nu = 0;
		bool errp = false;
		Vec3d normal = square.getNormal();
		for(int i=0; i<3; i++){
			if( normal[i] == 0 ) nz++;
			else{
				if( normal[i] == -1 ){
					posi = false;
					nu++;
				}
				else if( normal[i] == 1 ){
					posi = true;
					nu++;
				}
				else{
					std::cout << "error: not valid normal" << normal.toString() << std::endl;
					errp = true;
					break;
				}
			}
		}
		
		if( !(nu == 1 && nz == 2) && !errp ){
			std::cout << "error: not valid normal" << normal.toString() << std::endl;
		}
	}
	
	Vec3d hipoRes = square.getPosition() - square.getNormal();
	Vec3d toEmpty;
	if( posi ){
		toEmpty = hipoRes;
	}
	else{
		toEmpty = square.getPosition();
	}
	
	// Increase the size of the cube if necessary
	if( hipoRes[0] < aleft ){
		increaseWidth(-1);
	}
	else if( hipoRes[0] >= aright ){
		increaseWidth(+1);
	}
	else if( hipoRes[1] < abot ){
		increaseHeight(-1);
	}
	else if( hipoRes[1] >= atop ){
		increaseHeight(+1);
	}
	else if( hipoRes[2] < afront ){
		increaseDepth(-1);
	}
	else if( hipoRes[2] >= aback ){
		increaseDepth(+1);
	}
	
	// Empty the cube
	cube[toEmpty[0]-aleft][toEmpty[1]-abot][toEmpty[2]-afront] = 0;
	
	// update
	if( hipoRes[0] < left ) left--;
	if( hipoRes[0] >= right ) right++;
	if( hipoRes[1] < bot ) bot--;
	if( hipoRes[1] >= top ) top++;
	if( hipoRes[2] < front ) front--;
	if( hipoRes[2] >= back ) back++;
	
	// Check if there exists a cube behind
	Square res(square);
	res.setPosition( hipoRes );
	if( posi ){
		hipoRes = hipoRes - square.getNormal();
	}
	if(
		hipoRes[0] >= left &&
		hipoRes[0] < right &&
		hipoRes[1] >= bot &&
		hipoRes[1] < top &&
		hipoRes[2] >= front &&
		hipoRes[2] < back &&
		cube[hipoRes[0]-aleft][hipoRes[1]-abot][hipoRes[2]-afront] == 0
	){	// The next cube is empty
		res.setValid(false);
	}
	else{
		res.setValid(true);
	}
	
	return res;
	
}

Square CubeOfBits::pull(const Square& square){
	
	bool posi;	// positive pull
	{
		int nz = 0;
		int nu = 0;
		bool errp = false;
		Vec3d normal = square.getNormal();
		for(int i=0; i<3; i++){
			if( normal[i] == 0 ) nz++;
			else{
				if( normal[i] == -1 ){
					posi = false;
					nu++;
				}
				else if( normal[i] == 1 ){
					posi = true;
					nu++;
				}
				else{
					std::cout << "error: not valid normal" << std::endl;
					errp = true;
					break;
				}
			}
		}
		
		if( !(nu == 1 && nz == 2) && !errp ){
			std::cout << "error: not valid normal" << std::endl;
		}
	}
	
	Vec3d hipoRes = square.getPosition() + square.getNormal();
	Vec3d toFill;
	if( posi ){
		toFill = square.getPosition();
	}
	else{
		toFill = hipoRes;
	}
	
	// Fill the cube
	cube[toFill[0]-aleft][toFill[1]-abot][toFill[2]-afront] = 1;
	
	Vec3d normal = square.getNormal();
	
	// update left
	if( normal[0] == 1 && left == toFill[0] ){
		
		bool up = true;
		for(int i=front; i<back && up; i++)
		for(int j=bot; j<top && up; j++)
		if( cube[toFill[0]-aleft][j-abot][i-afront] == 0) up = false;
		
		if( up ) left++;
		
	}
	
	// update right
	if( normal[0] == -1 && right == toFill[0]+1 ){
		
		bool up = true;
		for(int i=front; i<back && up; i++)
		for(int j=bot; j<top && up; j++)
		if( cube[toFill[0]-aleft][j-abot][i-afront] == 0) up = false;
		
		if( up ) right--;
		
	}
	
	// update bot
	if( normal[1] == 1 && bot == toFill[1] ){
		
		bool up = true;
		for(int i=left; i<right && up; i++)
		for(int j=front; j<back && up; j++)
		if( cube[i-aleft][toFill[1]-abot][j-afront] == 0 ) up = false;
		
		if( up ) bot++;
		
	}
	
	// update top
	if( normal[1] == -1 && bot == toFill[1]+1 ){
		
		bool up = true;
		for(int i=left; i<right && up; i++)
		for(int j=front; j<back && up; j++)
		if( cube[i-aleft][toFill[1]-abot][j-afront] == 0 ) up = false;
		
		if( up ) top--;
		
	}
	
	// update front
	if( normal[2] == 1 && front == toFill[2] ){
		
		bool up = true;
		for(int i=left; i<right && up; i++)
		for(int j=bot; j<top && up; j++)
		if( cube[i-aleft][j-abot][toFill[2]-afront] == 0 ) up = false;
		
		if( up ) front++;
		
	}
	
	// update back
	if( normal[2] == -1 && back == toFill[2]+1 ){
		
		bool up = true;
		for(int i=left; i<right && up; i++)
		for(int j=bot; j<top && up; j++)
		if( cube[i-aleft][j-abot][toFill[2]-afront] == 0 ) up = false;
		
		if( up ) back--;
		
	}
	
	// Check if there exists a cube behind
	Square res(square);
	res.setPosition( hipoRes );
	if( !posi ){
		hipoRes = hipoRes + square.getNormal();
	}
	if(
		!(
			hipoRes[0] >= left &&
			hipoRes[0] < right &&
			hipoRes[1] >= bot &&
			hipoRes[1] < top &&
			hipoRes[2] >= front &&
			hipoRes[2] < back
		) ||
		cube[hipoRes[0]-aleft][hipoRes[1]-abot][hipoRes[2]-afront] == 1
	){	// The next cube is filled
		res.setValid(false);
	}
	else{
		res.setValid(true);
	}
	
	return res;
	
}

std::vector<Quad> CubeOfBits::getVisualRepresentation()const{
	
	std::vector<Quad> res;
	
	unsigned int colorCount = 0;
	
	// left to right
	
	for(int z=front; z<back; z++)
	for(int y=bot; y<top; y++){
		
		int status = 1;
		for(int x=left; x<right; x++){
			
			if(status == 0){
				
				if( cube[x-aleft][y-abot][z-afront] == 1 ){
					
					Quad q
					(
						Vec3d(x, y, z),
						Vec3d(-1, 0, 0),
						colorCount
					);
					res.push_back(q);
					colorCount++;
					status = 1;
					
				}
				
			}
			else{
				
				if( cube[x-aleft][y-abot][z-afront] == 0 ){
					
					Quad q
					(
						Vec3d(x, y, z),
						Vec3d(1, 0, 0),
						colorCount
					);
					res.push_back(q);
					colorCount++;
					status = 0;
					
				}
				
			}
			
		}
		
		if(status == 0){
			
			Quad q
			(
				Vec3d(right, y, z),
				Vec3d(-1, 0, 0),
				colorCount
			);
			res.push_back(q);
			colorCount++;
			
		}
		
	}
	
	// bot to top
	for(int x=left; x<right; x++)
	for(int z=front; z<back; z++){
		
		int status = 1;
		for(int y=bot; y<top; y++){
			
			if(status == 0){
				
				if( cube[x-aleft][y-abot][z-afront] == 1 ){
					
					Quad q
					(
						Vec3d(x, y, z),
						Vec3d(0, -1, 0),
						colorCount
					);
					res.push_back(q);
					colorCount++;
					status = 1;
									
				}
				
			}
			else{
				
				if( cube[x-aleft][y-abot][z-afront] == 0 ){
					
					Quad q
					(
						Vec3d(x, y, z),
						Vec3d(0, 1, 0),
						colorCount
					);
					res.push_back(q);
					colorCount++;
					status = 0;
					
				}
				
			}
		
		}
		
		if(status == 0){
			
			Quad q
			(
				Vec3d(x, top, z),
				Vec3d(0, -1, 0),
				colorCount
			);
			res.push_back(q);
			colorCount++;
			
		}
		
	}
		
	// front to back
	
	for(int x=left; x<right; x++)
	for(int y=bot; y<top; y++){
		
		int status = 1;
		for(int z=front; z<back; z++){
			
			if(status == 0){
			
				if( cube[x-aleft][y-abot][z-afront] == 1 ){
					
					Quad q
					(
						Vec3d(x, y, z),
						Vec3d(0, 0, -1),
						colorCount
					);
					res.push_back(q);
					colorCount++;
					status = 1;
				}
				
			}
			else{
				
				if( cube[x-aleft][y-abot][z-afront] == 0 ){
					
					Quad q
					(
						Vec3d(x, y, z),
						Vec3d(0, 0, 1),
						colorCount
					);
					res.push_back(q);
					colorCount++;
					status = 0;
					
				}
				
			}
			
		}
		
		if(status == 0){
			
			Quad q
			(
				Vec3d(x, y, back),
				Vec3d(0, 0, -1),
				colorCount
			);
			res.push_back(q);
			colorCount++;
			
		}
		
	}
	
	
	return res;
	
}

char CubeOfBits::operator()(int x, int y, int z)const{
	return cube[x-aleft][y-abot][z-afront];
}

char& CubeOfBits::operator()(int x, int y, int z){
	return cube[x-aleft][y-abot][z-afront];
}

std::string CubeOfBits::toString()const{
	
	std::stringstream ss;
	std::string res = "";
	for(int k=front; k<back; k++){
		for(int i=top-1; i>=bot; i--){
			
			for(int j=left; j<right; j++){
				ss << (int)(cube[j-aleft][i-abot][k-afront]) << " ";
			}
			ss << std::endl;
		}
		ss << "-------------" << std::endl;
	}
	
	res = ss.str();
	
	return res;
	
}

void CubeOfBits::save(string fileName)const{
	
	ofstream fs;
	fs.open(fileName.c_str(), ios::binary | ios::out );
	
	int width = right-left;
	int height = top-bot;
	int depth = back-front;
	
	fs << width << endl;
	fs << height << endl;
	fs << depth << endl;
	
	for(int x=left; x<right; x++)
	for(int y=bot; y<top; y++)
	for(int z=front; z<back; z++)
	{
		fs << (char)(cube[x-aleft][y-abot][z-afront] + '0') << " ";
	}
	
	fs.close();
	
}

// Private

void CubeOfBits::increaseWidth(int dir){
		
	char*** prevCube = cube;
	
	if( dir == -1 ){
		
		aleft -= ir;
		
		// reserve memory
		cube = new char**[aright-aleft];
		cube[0] = new char*[(atop-abot)*(aright-aleft)];
		cube[0][0] = new char[(atop-abot)*(aright-aleft)*(aback-afront)];
		
		// set everything to fill
		for(int i=0; i<(atop-abot)*(aright-aleft)*(aback-afront); i++){
			cube[0][0][i] = 1;
		}
		
		// make up the structure
		for(int i=0; i<(aright-aleft); i++){
			cube[i] = &cube[0][i*(atop-abot)];
			for(int j=0; j<(atop-abot); j++){
				cube[i][j] = &cube[0][0][ i*(atop-abot)*(aback-afront) + j*(aback-afront) ];
			}
		}
		
		// copy from the previous
		for(int i=ir; i<(aright-aleft); i++){
			for(int j=0; j<(atop-abot); j++){
				
				
				for(int k=0; k<(aback-afront); k++){
					
					cube[i][j][k] = prevCube[i-ir][j][k];
					
				}
				
			}
			
		}
		
		// delete previous
		delete[] prevCube[0][0];
		delete[] prevCube[0];
		delete[] prevCube;
		
	}
	else if( dir == +1 ){
		
		aright += ir;
		
		// reserve memory
		cube = new char**[aright-aleft];
		cube[0] = new char*[(atop-abot)*(aright-aleft)];
		cube[0][0] = new char[(atop-abot)*(aright-aleft)*(aback-afront)];
		
		// set everything to fill
		for(int i=0; i<(atop-abot)*(aright-aleft)*(aback-afront); i++){
			cube[0][0][i] = 1;
		}
		
		// make up the structure
		for(int i=0; i<(aright-aleft); i++){
			cube[i] = &cube[0][i*(atop-abot)];
			for(int j=0; j<(atop-abot); j++){
				cube[i][j] = &cube[0][0][ i*(atop-abot)*(aback-afront) + j*(aback-afront) ];
			}
		}
		
		// copy from the previous
		for(int i=0; i<(aright-aleft)-ir; i++){
			
			for(int j=0; j<(atop-abot); j++){
				
				
				for(int k=0; k<(aback-afront); k++){
					
					cube[i][j][k] = prevCube[i][j][k];
					
				}
				
			}
			
		}
		
		// delete previous
		delete[] prevCube[0][0];
		delete[] prevCube[0];
		delete[] prevCube;
		
	}
	else{
		// Error: the direction must be -1 or +1
	}
	
}

void CubeOfBits::increaseHeight(int dir){
	
	char*** prevCube = cube;
	
	if( dir == -1 ){
		
		abot -= ir;
		
		// reserve memory
		cube = new char**[aright-aleft];
		cube[0] = new char*[(atop-abot)*(aright-aleft)];
		cube[0][0] = new char[(atop-abot)*(aright-aleft)*(aback-afront)];
		
		// set everything to fill
		for(int i=0; i<(atop-abot)*(aright-aleft)*(aback-afront); i++){
			cube[0][0][i] = 1;
		}
		
		// make up the structure
		for(int i=0; i<(aright-aleft); i++){
			cube[i] = &cube[0][i*(atop-abot)];
			for(int j=0; j<(atop-abot); j++){
				cube[i][j] = &cube[0][0][ i*(atop-abot)*(aback-afront) + j*(aback-afront) ];
			}
		}
		
		// copy from the previous
		for(int i=0; i<(aright-aleft); i++){
			
			for(int j=ir; j<(atop-abot); j++){
				
				
				for(int k=0; k<(aback-afront); k++){
					
					cube[i][j][k] = prevCube[i][j-ir][k];
					
				}
				
			}
			
		}
		
		// delete previous
		delete[] prevCube[0][0];
		delete[] prevCube[0];
		delete[] prevCube;
		
		
	}
	else if( dir == +1 ){
		
		atop += ir;
		
		// reserve memory
		cube = new char**[aright-aleft];
		cube[0] = new char*[(atop-abot)*(aright-aleft)];
		cube[0][0] = new char[(atop-abot)*(aright-aleft)*(aback-afront)];
		
		// set everything to fill
		for(int i=0; i<(atop-abot)*(aright-aleft)*(aback-afront); i++){
			cube[0][0][i] = 1;
		}
		
		// make up the structure
		for(int i=0; i<(aright-aleft); i++){
			cube[i] = &cube[0][i*(atop-abot)];
			for(int j=0; j<(atop-abot); j++){
				cube[i][j] = &cube[0][0][ i*(atop-abot)*(aback-afront) + j*(aback-afront) ];
			}
		}
		
		// copy from the previous
		for(int i=0; i<(aright-aleft); i++){
			
			for(int j=0; j<(atop-abot)-ir; j++){
				
				
				for(int k=0; k<(aback-afront); k++){
					
					cube[i][j][k] = prevCube[i][j][k];
					
				}
				
			}
			
		}
		
		// delete previous
		delete[] prevCube[0][0];
		delete[] prevCube[0];
		delete[] prevCube;
		
		
	}
	else{
		// Error: the direction must be -1 or +1
	}
	
}

void CubeOfBits::increaseDepth(int dir){
	
	char*** prevCube = cube;
	
	if( dir == -1 ){
		
		afront -= ir;
		
		// reserve memory
		cube = new char**[aright-aleft];
		cube[0] = new char*[(atop-abot)*(aright-aleft)];
		cube[0][0] = new char[(atop-abot)*(aright-aleft)*(aback-afront)];
		
		// set everything to fill
		for(int i=0; i<(atop-abot)*(aright-aleft)*(aback-afront); i++){
			cube[0][0][i] = 1;
		}
		
		// make up the structure
		for(int i=0; i<(aright-aleft); i++){
			cube[i] = &cube[0][i*(atop-abot)];
			for(int j=0; j<(atop-abot); j++){
				cube[i][j] = &cube[0][0][ i*(atop-abot)*(aback-afront) + j*(aback-afront) ];
			}
		}
		
		// copy from the previous
		for(int i=0; i<(aright-aleft); i++){
			
			for(int j=0; j<(atop-abot); j++){
				
				
				for(int k=ir; k<(aback-afront); k++){
					
					cube[i][j][k] = prevCube[i][j][k-ir];
					
				}
				
			}
			
		}
		
		// delete previous
		delete[] prevCube[0][0];
		delete[] prevCube[0];
		delete[] prevCube;
		
	}
	else if( dir == +1 ){
		
		aback += ir;
		
		// reserve memory
		cube = new char**[aright-aleft];
		cube[0] = new char*[(atop-abot)*(aright-aleft)];
		cube[0][0] = new char[(atop-abot)*(aright-aleft)*(aback-afront)];
		
		// set everything to fill
		for(int i=0; i<(atop-abot)*(aright-aleft)*(aback-afront); i++){
			cube[0][0][i] = 1;
		}
		
		// make up the structure
		for(int i=0; i<(aright-aleft); i++){
			cube[i] = &cube[0][i*(atop-abot)];
			for(int j=0; j<(atop-abot); j++){
				cube[i][j] = &cube[0][0][ i*(atop-abot)*(aback-afront) + j*(aback-afront) ];
			}
		}
		
		// copy from the previous
		for(int i=0; i<(aright-aleft); i++){
			
			for(int j=0; j<(atop-abot); j++){
				
				
				for(int k=0; k<(aback-afront)-ir; k++){
					
					cube[i][j][k] = prevCube[i][j][k];
					
				}
				
			}
			
		}
		
		// delete previous
		delete[] prevCube[0][0];
		delete[] prevCube[0];
		delete[] prevCube;
		
		
	}
	else{
		// Error: the direction must be -1 or +1
	}
	
}
