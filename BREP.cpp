#include "BREP.hpp"
#include <map>
#include <iostream>
#include <vector>
#include <tuple>
#include <algorithm>	// for swap

using namespace std;

BREP::BREP(const CubeOfBits& cob){
	
	this->cob = cob;
	
	int left, right, top, bot, front, back;
	left = cob.getLeft(); right = cob.getRight();
	bot = cob.getBot(); top = cob.getTop();
	front = cob.getFront(); back = cob.getBack();
	
	// invert bits of the local cob
	for(int i=left; i<right; i++)
	for(int j=bot; j<top; j++)
	for(int k=front; k<back; k++)
	{
		
		// if 0 change to 1, if 1 change to 0
		this->cob(i, j, k) = (this->cob(i, j, k)+1) & (1);
		
	}
		
	// GET SLICES
	vector<Slice> slicesX, slicesY, slicesZ;
	
	// X-axis slices
	slicesX = Slice::getSlicesX(this->cob);
	
	// Y-axis slices
	slicesY = Slice::getSlicesY(this->cob);
	
	// Z-axis slices
	slicesZ = Slice::getSlicesZ(this->cob);
	
	// compute the set of vertices
	map<Vertex, int> setOfVerticesMap;
	// in order to obtain the set of vertices we will use
	// the X-axis slices, but we could have chosen any other
	for(int k=0; k<slicesX.size(); k++){
		
		const Slice& slice = slicesX[k];
		
		// first col
		bool line = false;
		for(int j=0; j<slice.getHeight(); j++){
			
			if(slice(0, j) == 0){
				
				if(line){
					setOfVerticesMap[Vertex(k, j, 0)] = 1;
				}
				line = false;
				
			}
			else{
				
				if(!line){
					setOfVerticesMap[Vertex(k, j, 0)] = 1;
				}
				line = true;
				
			}
			
		}
		if(line) setOfVerticesMap[Vertex(k, slice.getHeight(), 0)] = 1;
		
		// for the rest of cols
		
		for(int i=1; i<slice.getWidth(); i++){
			
			bool line = false;	// indicates wether we are following a line
			
			for(int j=0; j<slice.getHeight(); j++){
				
				if(slice(i-1, j) == slice(i, j)){
					
					if(line){
						setOfVerticesMap[Vertex(k, j, i)] = 1;
					}
					line = false;
					
				}
				else{
					
					if(!line){
						setOfVerticesMap[Vertex(k, j, i)] = 1;
					}
					line = true;
					
				}
				
			}
			if(line) setOfVerticesMap[Vertex(k, slice.getHeight(), i)] = 1;
		
		}
		
		
		// for the last col
		
		line = false;
		for(int j=0; j<slice.getHeight(); j++){
			
			if(slice(slice.getWidth()-1, j) == 0){
				
				if(line){
					setOfVerticesMap[Vertex(k, j, slice.getWidth())] = 1;
				}
				line = false;
				
			}
			else{
				
				if(!line){
					setOfVerticesMap[Vertex(k, j, slice.getWidth())] = 1;
				}
				line = true;
				
			}
			
		}
		if(line) setOfVerticesMap[Vertex(k, slice.getHeight(), slice.getWidth())] = 1;
		
		
	}
	
	for(int i=0; i<slicesX.size(); i++) cout << slicesX[i].toString() << endl << endl;
	
	// at this poit we should have the whole set of vertices
	
	// trace vertices
	cout << endl << "  Vertices" << endl;
	cout << "------------" << endl;
	for(
		map<Vertex, int>::iterator it = setOfVerticesMap.begin();
		it != setOfVerticesMap.end();
		++it
	)
	{
		cout << "(" << it->first.x << "," << it->first.y << "," << it->first.z << ")" << endl;
	}
	
	// let's assign a number to each vertex
	int count = 0;
	for(
		map<Vertex, int>::iterator it = setOfVerticesMap.begin();
		it != setOfVerticesMap.end();
		++it
	)
	{
		
		it->second = count;
		count++;
		
	}
	
	
	
	// Detect edges
	// in order to detect edges we need to use the slices of two axis
	// I have chosen axis x and y arbitrarily
	
	// the set of edges (0:vert 0, 1: vert 1, 2: normal of the face)
	map<tuple<int,int,int>, int> setOfEdgesMap;
	
	// get edges from x-axis slices
	for(int k=0; k<slicesX.size(); k++){
		
		const Slice& slice = slicesX[k];
		
		// vertical sweep
		for(int i=-1; i<slice.getWidth(); i++){
			
			int startl, startr;	// indicates where the line started
			bool linel = false, liner = false;	// indicates if we are folliwing a line
			
			int prevl = 0, prevr = 0;	// previuos left and previous right
			for(int j=0; j<slice.getHeight(); j++){
				
				int left, right;
				
				if( i<0 ) left = 0;
				else left = slice(i, j);
				
				if( i+1 == slice.getWidth() ) right = 0;
				else right = slice(i+1, j);
				
				// check the left
				if( prevl != left ){
					
					// we have stopped following the line
					if( linel ){
						
						int min_vert, max_vert;
						min_vert = startl;
						max_vert = setOfVerticesMap[ Vertex(j, k, i+1) ];
						if( max_vert < min_vert ) swap(min_vert, max_vert);
						
						int norm;	// normal
						if( prevl == 0 ){	// there is a hole
							norm = -1;
							norm *= left;
						}
						else{
							norm = prevl;
						}
						setOfEdgesMap[ make_tuple(min_vert, max_vert, norm) ] = 1;
						
						linel = false;
						
					}
					
					// we started following a line
					else if( left != right ){
						
						startl = setOfVerticesMap[ Vertex(j, k, i+1) ];
						
						linel = true;
						
					}
					
				}
				else{
					// nothing to do here
				}
				
				// check the right
				if( prevr != right ){
					
					// we have stopped following the line
					if( liner ){
						int min_vert, max_vert;
						min_vert = startr;
						max_vert = setOfVerticesMap[ Vertex(j, k, i+1) ];
						if( max_vert < min_vert ) swap(min_vert, max_vert);
						
						int norm;	// normal
						if( prevr == 0 ){	// there is a hole
							norm = -1;
							norm *= right;
						}
						else{
							norm = prevr;
						}
						setOfEdgesMap[ make_tuple(min_vert, max_vert, norm) ] = 1;
						
						liner = false;
						
					}
					
					// we started following a line
					else if( left != right ){
						
						startr = setOfVerticesMap[ Vertex(j, k, i+1) ];
						
						liner = true;
						
					}
					
				}
				else{
					// nothing to do here
				}
				
				
				prevl = left;
				prevr = right;
				
			}

			
		}
		
		// horizontal sweep-----------
		for(int i=-1; i<slice.getHeight(); i++){
			
			bool line = false;
			int prev;
			
			for(int j=0; j<slice.getWidth(); j++){
				
				int top, bot;
				
				if( i<0 ) top = 0;
				else top = slice(j, i);
				
				if( i+1 == slice.getHeight() ) bot = 0;
				else bot = slice(j, i+1);
				
				
				
			}
			
		}
		
	}
	
	// get edges from y-axis slices
	for(int k=0; k<slicesY.size(); k++){
		
		const Slice& slice = slicesY[k];
		
		// vertical sweep
		for(int i=-1; i<slice.getWidth(); i++){
			
			bool line = false;
			int prev;
			
			for(int j=0; j<slice.getHeight(); j++){
				
				int left, right;
				
				if( i<0 ) left = 0;
				else left = slice(i, j);
				
				if( i+1 == slice.getWidth() ) right = 0;
				else right = slice(i+1, j);
				
				if( right == left ){
					
					if(line){
						
						int min_vert, max_vert;
						min_vert = prev;
						max_vert = setOfVerticesMap[ Vertex(i+1, k, j) ];
						if( max_vert < min_vert ) swap(min_vert, max_vert);
						setOfEdgesMap[ make_tuple(min_vert, max_vert) ] = 1;
						
					}
					line = false;
					
				}
				else{
					
					if(!line){
						
						prev = setOfVerticesMap[ Vertex(i+1, k, j) ];
						
					}
					line = true;
					
				}
				
			}
			if(line){
				int min_vert, max_vert;
				min_vert = prev;
				max_vert = setOfVerticesMap[ Vertex(i+1, k, slice.getHeight()) ];
				if( max_vert < min_vert ) swap(min_vert, max_vert);
				setOfEdgesMap[ make_tuple(min_vert, max_vert) ] = 1;
			}
			
		}
		
		// horizontal sweep
		for(int i=-1; i<slice.getHeight(); i++){
			
			bool line = false;
			int prev;
			
			for(int j=0; j<slice.getWidth(); j++){
				
				int top, bot;
				
				if( i<0 ) top = 0;
				else top = slice(j, i);
				
				if( i+1 == slice.getHeight() ) bot = 0;
				else bot = slice(j, i+1);
				
				if( top == bot ){
					
					if(line){
						
						int min_vert, max_vert;
						min_vert = prev;
						max_vert = setOfVerticesMap[ Vertex(j, k, i+1) ];
						if( max_vert < min_vert ) swap(min_vert, max_vert);
						setOfEdgesMap[ make_tuple(min_vert, max_vert) ] = 1;
						
					}
					line = false;
					
				}
				else{
					
					if(!line){
						
						prev = setOfVerticesMap[ Vertex(j, k, i+1) ];
						
					}
					line = true;
					
				}
				
			}
			if(line){
				int min_vert, max_vert;
				min_vert = prev;
				max_vert = setOfVerticesMap[ Vertex(slice.getWidth(), k, i+1) ];
				if( max_vert < min_vert ) swap(min_vert, max_vert);
				setOfEdgesMap[ make_tuple(min_vert, max_vert) ] = 1;
			}
			
		}
		
	}
	
	// trace edges
	cout << endl << "   Edges" << endl;
	cout << "------------" << endl;
	for
	(
		map<tuple<int,int>, int>::iterator it = setOfEdgesMap.begin();
		it != setOfEdgesMap.end();
		++it
	)
	{
		cout << get<0>( it->first ) << ", " << get<1>( it->first ) << endl;
	}
	
	// assign a number to each edge as an ID
	count = 0;
	for
	(
		map<tuple<int,int>, int>::iterator it = setOfEdgesMap.begin();
		it != setOfEdgesMap.end();
		++it
	)
	{
		it->second = count;
		count++;
	}
	
	// FACES detection
	// faces detection is much more complex. We need to sweep
	// over all slices of x, y and z axis
	
	// get faces which are orthogonal to the x axis
	for(int k=0; k<slicesX.size(); k++){
		
		// sweep vertically(we could have chosen horizontally)
		
		// list of edges
		list<tuplue<>>
		
		for(int i=-1; i<slicesX; i++)
		
	}
	
}

void BREP::save(string fileName){
	
	
	
}
