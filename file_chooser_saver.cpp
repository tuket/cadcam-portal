#include "file_chooser_saver.hpp"

Gtk::Main FileChooserSaver::kit(false);

std::string FileChooserSaver::getFileName() {
	FileChooserSaver dialog("Save file", Gtk::FILE_CHOOSER_ACTION_SAVE);
	kit.run(dialog);
	std::string ret = dialog.chosenFile;
	return ret;
}

void FileChooserSaver::on_my_response(int response_id) {
	if( response_id == -5 ) chosenFile = get_filename();
	else chosenFile = "";
	hide();
}
