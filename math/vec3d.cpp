#include "vec3d.hpp"
#include <cmath>
#include <sstream>

using namespace std;

Vec3d::Vec3d(){
	vec[0] = vec[1] = vec[2] = 0;
}

Vec3d::Vec3d(const Vec3d& vec){
	this->vec[0] = vec[0];
	this->vec[1] = vec[1];
	this->vec[2] = vec[2];
}

Vec3d::Vec3d(int x, int y, int z){
	vec[0] = x;
	vec[1] = y;
	vec[2] = z;
}

Vec3d Vec3d::operator+(const Vec3d& v)const{
	
	Vec3d res(*this);
	res[0] += v[0];
	res[1] += v[1];
	res[2] += v[2];
	return res;
	
}

Vec3d Vec3d::operator-(const Vec3d& v)const{
	
	Vec3d res(*this);
	res[0] -= v[0];
	res[1] -= v[1];
	res[2] -= v[2];
	return res;
	
}

Vec3d Vec3d::operator-()const{
	Vec3d res(-vec[0], -vec[1], -vec[2]);
	return res;
}

Vec3d Vec3d::operator=(const Vec3d& v){
	
	vec[0] = v[0];
	vec[1] = v[1];
	vec[2] = v[2];
	return(*this);
	
}

bool Vec3d::operator==(const Vec3d& v)const{
	return
		vec[0] == v.vec[0] &&
		vec[1] == v.vec[1] &&
		vec[2] == v.vec[2]
	;
}

Vec3d Vec3d::operator*(int s)const{
	Vec3d res(s*vec[0], s*vec[1], s*vec[2]);
	return res;
}

int Vec3d::operator*(const Vec3d& v)const{
	return vec[0]*v[0]+vec[1]*v[1]+vec[2]*v[2];
}

int Vec3d::getSquaredLength()const{
	return vec[0]*vec[0]+vec[1]*vec[1]+vec[2]*vec[2];
}

int Vec3d::getSquaredMagnitude()const{
	return vec[0]*vec[0]+vec[1]*vec[1]+vec[2]*vec[2];
}

int Vec3d::getSquaredNorm()const{
	return vec[0]*vec[0]+vec[1]*vec[1]+vec[2]*vec[2];
}

float Vec3d::getLength()const{
	sqrt(getSquaredLength());
}

float Vec3d::getMagnitude()const{
	sqrt(getSquaredLength());
}

float Vec3d::getNorm()const{
	sqrt(getSquaredLength());
}

Vec3d Vec3d::crossProduct(const Vec3d& v1, const Vec3d& v2){
	Vec3d res(v1[1]*v2[2]-v1[2]*v2[1], v1[2]*v2[0]-v1[0]*v2[2], v1[0]*v2[1]-v1[1]*v2[0]);
	return res;
}

int Vec3d::dotProduct(const Vec3d& v1, const Vec3d& v2){
	return v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2];
}

float Vec3d::angleBetween(const Vec3d& v1, const Vec3d& v2){
	float dp = dotProduct(v1, v2);
	float len1 = v1.getLength();
	float len2 = v2.getLength();
	return acos( dp/(len1*len2) );
}

int Vec3d::operator[](int i)const{
	return vec[i];
}

int& Vec3d::operator[](int i){
	return vec[i];
}

string Vec3d::toString()const{
	stringstream ss;
	ss << "(" << vec[0] << "," << vec[1] << "," << vec[2] << ")";
	return ss.str();
}
