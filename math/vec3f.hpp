
#ifndef VEC3F
#define VEC3F

class Vec3f{
	
	float vec[3];
	
public:

	Vec3f();
	Vec3f(float x, float y, float z);
	Vec3f(const Vec3f& v);
	
	Vec3f operator=(const Vec3f& v);
	
	float& operator[](int i);
	const float& operator[](int i)const;
	
	Vec3f operator+(const Vec3f& v)const;
	Vec3f operator-(const Vec3f& v)const;
	
	Vec3f operator+=(const Vec3f& v);
	Vec3f operator-=(const Vec3f& v);
	
	Vec3f operator*(float x)const;
	float operator*(const Vec3f& v)const;
	Vec3f operator/(float x)const;
	
	float getX()const;
	float getY()const;
	float getZ()const;
	
	void setX(float x);
	void setY(float y);
	void setZ(float z);
	
};

Vec3f operator*(float x, const Vec3f& v);

#endif
