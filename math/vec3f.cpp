#include "vec3f.hpp"
#include <cmath>

Vec3f::Vec3f(){
	vec[0] = vec[1] = vec[2] = 0;
}

Vec3f::Vec3f(float x, float y, float z){
	vec[0] = x;
	vec[1] = y;
	vec[2] = z;
}

Vec3f::Vec3f(const Vec3f& v){
	vec[0] = v[0];
	vec[1] = v[1];
	vec[2] = v[2];
}

Vec3f Vec3f::operator=(const Vec3f& v){
	vec[0] = v[0];
	vec[1] = v[1];
	vec[2] = v[2];
	return *this;
}

float& Vec3f::operator[](int i){
	return vec[i];
}

const float& Vec3f::operator[](int i)const{
	return vec[i];
}

Vec3f Vec3f::operator+(const Vec3f& v)const{
	Vec3f res(*this);
	res[0] += v[0];
	res[1] += v[1];
	res[2] += v[2];
	return res;
}

Vec3f Vec3f::operator-(const Vec3f& v)const{
	Vec3f res(*this);
	res[0] -= v[0];
	res[1] -= v[1];
	res[2] -= v[2];
	return res;
}

Vec3f Vec3f::operator+=(const Vec3f& v){
	vec[0] += v[0];
	vec[1] += v[1];
	vec[2] += v[2];
	return *this;
}

Vec3f Vec3f::operator-=(const Vec3f& v){
	vec[0] -= v[0];
	vec[1] -= v[1];
	vec[2] -= v[2];
	return *this;
}

Vec3f Vec3f::operator*(float x)const{
	Vec3f res(*this);
	res[0] *= x;
	res[1] *= x;
	res[2] *= x;
	return res;
}

float Vec3f::operator*(const Vec3f& v)const{
	return vec[0]*v[0] + vec[1]*v[1] + vec[2]*v[2];
}

Vec3f Vec3f::operator/(float x)const{
	Vec3f res(*this);
	res[0] /= x;
	res[1] /= x;
	res[2] /= x;
	return res;
}

float Vec3f::getX()const{
	return vec[0];
}

float Vec3f::getY()const{
	return vec[1];
}

float Vec3f::getZ()const{
	return vec[2];
}

void Vec3f::setX(float x){
	vec[0] = x;
}

void Vec3f::setY(float y){
	vec[1] = y;
}

void Vec3f::setZ(float z){
	vec[2] = z;
}
