#include <string>

#ifndef VEC3D
#define VEC3D

class Vec3d{

protected:
	
	int vec[3];

public:
	
	Vec3d();
	Vec3d(const Vec3d& vec);
	Vec3d(int x, int y, int z);
	
	int getX(){ return vec[0]; }
	int getY(){ return vec[1]; }
	int getZ(){ return vec[2]; }
	
	void setX(int x){ vec[0] = x; }
	void setY(int y){ vec[1] = y; }
	void setZ(int z){ vec[2] = z; }
	
	Vec3d operator+(const Vec3d& v)const;
	Vec3d operator-(const Vec3d& v)const;
	Vec3d operator-()const;
	
	Vec3d operator=(const Vec3d& v);
	bool operator==(const Vec3d& v)const;
	
	Vec3d operator*(int s)const;
	
	int operator*(const Vec3d& v)const;
	
	float getLength()const;
	float getMagnitude()const;
	float getNorm()const;
	
	int getSquaredLength()const;
	int getSquaredMagnitude()const;
	int getSquaredNorm()const;
		
	static Vec3d crossProduct(const Vec3d& v1, const Vec3d& v2);
	static int dotProduct(const Vec3d& v1, const Vec3d& v2);
	static float angleBetween(const Vec3d& v1, const Vec3d& v2);	// in radians
	
	int operator[](int i)const;
	int& operator[](int i);
	
	std::string toString()const;
	
};

#endif
