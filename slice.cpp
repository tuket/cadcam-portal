#include "slice.hpp"

#include <sstream>

using namespace std;

Slice::Slice(int width, int height){
	
	this->width = width;
	this->height = height;
	
	cell = new char*[height];
	cell[0] = new char[width*height];
	for(int i=0; i<height; i++) cell[i] = &cell[0][i*width];
	
	for(int i=0; i<width; i++)
	for(int j=0; j<height; j++) cell[j][i] = 0;
	
}

Slice::Slice(const Slice& slice){
	
	this->width = slice.width;
	this->height = slice.height;
	
	cell = new char*[height];
	cell[0] = new char[width*height];
	for(int i=0; i<height; i++) cell[i] = &cell[0][i*width];
	
	for(int i=0; i<width; i++)
	for(int j=0; j<height; j++) cell[j][i] = slice.cell[j][i];
	
}

int Slice::getWidth()const{
	
	return width;
	
}

int Slice::getHeight()const{
	
	return height;
	
}

char& Slice::operator()(int x, int y){
	
	return cell[y][x];
	
}

const char& Slice::operator()(int x, int y)const{
	
	return cell[y][x];
	
}

string Slice::toString()const{
	
	stringstream ss;
	
	string mmap[3] = {"-1", " 0", " 1"};
	
	for(int i=0; i<height-1; i++){
		for(int j=0; j<width-1; j++){
			ss << mmap[cell[i][j]+1] << " ";
		}
		ss << mmap[cell[i][width-1]+1];
		ss << endl;
	}
	for(int j=0; j<width-1; j++){
		ss << mmap[cell[height-1][j]+1] << " ";
	}
	ss << mmap[cell[height-1][width-1]+1];
	
	return ss.str();
	
}

Slice::~Slice(){
	
	delete[] cell[0];
	delete[] cell;
	
}

// static

vector<Slice> Slice::getSlicesX(const CubeOfBits& cob){
	
	int left, right, top, bot, front, back;
	left = cob.getLeft(); right = cob.getRight();
	bot = cob.getBot(); top = cob.getTop();
	front = cob.getFront(); back = cob.getBack();
	
	// this is the result
	vector<Slice> slicesX;
	
	Slice slice(back-front, top-bot);
	
	for(int i=bot; i<top; i++)
	for(int j=front; j<back; j++)
	{
		
		slice(j, i) = 0 - cob(left, i, j);
		
	}
	slicesX.push_back(slice);
	
	for(int k=left+1; k<right; k++)
	{
		
		for(int i=bot; i<top; i++)
		for(int j=front; j<back; j++)
		{
			
			slice(j, i) = cob(k-1, i, j) - cob(k, i, j);
			
		}
		slicesX.push_back(slice);
		
	}
	
	for(int i=bot; i<top; i++)
	for(int j=front; j<back; j++)
	{
		
		slice(j, i) = cob(right-1, i, j) - 0;
		
	}
	slicesX.push_back(slice);
	
	return slicesX;
	
}

vector<Slice> Slice::getSlicesY(const CubeOfBits& cob){
	
	int left, right, top, bot, front, back;
	left = cob.getLeft(); right = cob.getRight();
	bot = cob.getBot(); top = cob.getTop();
	front = cob.getFront(); back = cob.getBack();
	
	// this is the result
	vector<Slice> slicesY;
	
	Slice slice(right-left, back-front);
	
	for(int i=front; i<back; i++)
	for(int j=left; j<right; j++)
	{
		
		slice(j, i) = 0 - cob(j, bot, i);
		
	}
	slicesY.push_back(slice);
	
	for(int k=bot+1; k<top; k++){
		
		for(int i=front; i<back; i++)
		for(int j=left; j<right; j++)
		{
			
			slice(j, i) = cob(j, k-1, i) - cob(j, k, i);
			
		}
		slicesY.push_back(slice);
		
	}
	
	for(int i=front; i<back; i++)
	for(int j=left; j<right; j++)
	{
		
		slice(j, i) = cob(j, top-1, i) - 0;
		
	}
	slicesY.push_back(slice);
	
	return slicesY;
	
}

vector<Slice> Slice::getSlicesZ(const CubeOfBits& cob){
	
	int left, right, top, bot, front, back;
	left = cob.getLeft(); right = cob.getRight();
	bot = cob.getBot(); top = cob.getTop();
	front = cob.getFront(); back = cob.getBack();
	
	// this is the result
	vector<Slice> slicesZ;
	
	Slice slice(right-left, top-bot);
	
	for(int i=left; i<right; i++)
	for(int j=bot; j<top; j++)
	{
		
		slice(i, j) = 0 - cob(i, j, front);
		
	}
	slicesZ.push_back(slice);
	
	for(int k=front+1; k<back; k++){
		
		for(int i=left; i<right; i++)
		for(int j=bot; j<top; j++)
		{
			
			slice(i, j) = cob(i, j, k-1) - cob(i, j, k);
			
		}
		slicesZ.push_back(slice);
		
	}
	
	for(int i=left; i<right; i++)
	for(int j=bot; j<top; j++)
	{
		
		slice(i, j) = cob(i, j, back-1) - 0;
		
	}
	slicesZ.push_back(slice);
	
	return slicesZ;
	
}
