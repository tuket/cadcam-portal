#include "math/vec3f.hpp"

#ifndef CAMERA
#define CAMERA

class Camera{
	
	Vec3f pos;
	float heading;
	float pitch;
	float _near;
	float _far;

public:
	
	Camera();
	Camera(const Vec3f& pos);
	Camera(const Vec3f& pos, float heading, float pitch);
	
	Vec3f getPosition()const;
	void setPosition(const Vec3f& pos);
	
	float getHeading()const;
	float getPitch()const;
	
	void setHeading(float a);
	void setPitch(float a);
	
	float getNear()const;
	float getFar()const;
	
	void setNear(float _near);
	void setFar(float _far);
	
	Vec3f getDirection()const;
	
	
};

#endif
