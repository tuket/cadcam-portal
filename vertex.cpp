#include "vertex.hpp"

Vertex::Vertex(int x, int y, int z){
	
	this->x = x;
	this->y = y;
	this->z = z;
	
}

Vertex::Vertex(const Vertex& vert){
	
	x = vert.x;
	y = vert.y;
	z = vert.z;
	
}

bool Vertex::operator==(const Vertex& vert)const{
	
	return
	(
		x == vert.x &&
		y == vert.y &&
		z == vert.z
	);
	
}

bool Vertex::operator<(const Vertex& vert)const{
	
	if( x == vert.x ){
		
		if( y == vert.y ) return z < vert.z;
		else return y < vert.y;
		
	}
	else return x < vert.x;
	
}
