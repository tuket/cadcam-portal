#include "../cube_of_bits.hpp"
#include <iostream>
#include <vector>

using namespace std;

int main(){
	
	CubeOfBits cob;
	
	vector<Quad> vq = cob.getVisualRepresentation();
	
	for(int i=0; i<vq.size(); i++){
		
		cout << vq[i].toString() << endl;
		
	}
	
}
	
