#include "../slice.hpp"
#include "../BREP.hpp"
#include "../cube_of_bits.hpp"
#include <iostream>
#include <vector>

using namespace std;

void printSliceVec(const vector<Slice>& v);

int main(){
	
	CubeOfBits cob;
	
	int left, right, bot, top, front, back;
	left = cob.getLeft(); right = cob.getRight();
	bot = cob.getBot(); top = cob.getTop();
	front = cob.getFront(); back = cob.getBack();
	
	// invert the cob
	for(int i=left; i<right; i++)
	for(int j=bot; j<top; j++)
	for(int k=front; k<back; k++)
	{
		cob(i, j, k) = (cob(i, j, k)+1) & 1;
	}
	
	vector<Slice> slicesX = Slice::getSlicesX(cob);
	
	printSliceVec(slicesX);
	
}

void printSliceVec(const vector<Slice>& v){
	
	for(int i=0; i<v.size(); i++){
		
		cout << v[i].toString() << endl << endl;
		
	}
	
}
