#include <iostream>
#include <vector>
#include "../cube_of_bits.hpp"
#include "../BREP.hpp"

using namespace std;

int main(){
	
	CubeOfBits cob;	
	
	cob.pull(Square(Vec3d(0,0,0),Vec3d(1,0,0)));
	
	BREP brep(cob);
	
}
