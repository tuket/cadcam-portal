#include "../cube_of_bits.hpp"
#include <iostream>

using namespace std;

int main(){
	
	CubeOfBits cob;
	
	Square sq(Vec3d(1, 0, 0), Vec3d(0, 1, 0));
	cob.push(sq);
	
	sq = Square(Vec3d(0, 0, 0), Vec3d(1, 0, 0));
	cob.push(sq);
	
	sq = Square(Vec3d(-1, 0, 0), Vec3d(0, 1, 0));
	cob.push(sq);
	
	sq = Square(Vec3d(1, -1, 0), Vec3d(1, 0, 0));
	sq = cob.push(sq);
	
	cout << cob.toString() << endl;
	cout << sq.toString() << endl;
	
}
