#include "file_chooser.hpp"

Gtk::Main FileChooser::kit(false);

std::string FileChooser::getFileName() {
	FileChooser dialog("Open file", Gtk::FILE_CHOOSER_ACTION_OPEN);
	kit.run(dialog);
	std::string ret = dialog.chosenFile;
	return ret;
}

void FileChooser::on_my_response(int response_id) {
	if( response_id == -5 ) chosenFile = get_filename();
	else chosenFile = "";
	hide();
}
