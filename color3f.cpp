#include "color3f.hpp"

Color3f::Color3f(){
	r = g = b = 0.f;
}

Color3f::Color3f(float r, float g, float b){
	this->r = r;
	this->g = g;
	this->b = b;
}

Color3f::Color3f(unsigned int c){
	
	unsigned int r, g, b;
	
	r = (c & ( 0xFF0000 )) >> 16;
	g = (c & ( 0XFF00 )) >> 8;
	b = c & ( 0xFF );
	
	this->r = r/255.f;
	this->g = g/255.f;
	this->b = b/255.f;
	
}

Color3f::Color3f(unsigned int r, unsigned int g, unsigned int b){
	
	this->r = r/255.f;
	this->g = g/255.f;
	this->b = b/255.f;
	
}

float Color3f::getR()const{
	return r;
}

float Color3f::getG()const{
	return g;
}

float Color3f::getB()const{
	return b;
}

unsigned int Color3f::getIntR()const{
	return r*255;
}

unsigned int Color3f::getIntG()const{
	return g*255;
}

unsigned int Color3f::getIntB()const{
	return b*255;
}

unsigned int Color3f::getInt()const{
	unsigned char r = (this->r)*255;
	unsigned char g = (this->g)*255;
	unsigned char b = (this->b)*255;
	unsigned int res = r;
	res = res << 8;
	res |= g;
	res = res << 8;
	res |= b;
	return res;
}
