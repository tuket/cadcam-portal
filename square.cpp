#include "square.hpp"
#include <sstream>

using namespace std;

Square::Square(const Vec3d& position, const Vec3d& normal){
	
	valid = true;
	this->position = position;
	this->normal = normal;
	
}

Vec3d Square::getPosition()const{
	return position;
}

Vec3d Square::getNormal()const{
	return normal;
}

void Square::setPosition(const Vec3d& position){
	this->position = position;
}

void Square::setNormal(const Vec3d& normal){
	this->normal = normal;
}

bool Square::getValid(){
	return valid;
}

void Square::setValid(bool valid){
	this->valid = valid;
}

bool Square::operator==(const Square& s)const{
	return
		position == s.position &&
		normal == s.normal &&
		valid == s.valid
	;
}

string Square::toString(){
	
	stringstream ss;
	ss << "( pos: ("
		<< position[0] << "," << position[1] << "," << position[2]
	<< ")"
	<< ", nor: ("
		<< normal[0] << "," << normal[1] << "," << normal[2]
	<< "), "
	<< "valid: " << ((valid)?"true":"false")
	<< " )";
	
	return ss.str();
	
}
