#include "math/vec3d.hpp"
#include "square.hpp"
#include "quad.hpp"
#include <vector>
#include <string>

#ifndef CUBE_OF_BITS
#define CUBE_OF_BITS

class CubeOfBits{
	
	int bot, top;		// dimensions of the cube(min)
	int back, front;
	int left, right;
	
	char*** cube;
	int abot, atop;		// dimensions of the array
	int aback, afront;
	int aleft, aright;

public:
	
	CubeOfBits();
	CubeOfBits(unsigned int width, unsigned int height, unsigned int depth);
	CubeOfBits(std::string fileName);
	
	Square push(const Square& square);
	Square pull(const Square& square);
	
	std::vector<Quad> getVisualRepresentation()const;
	
	char operator()(int x, int y, int z)const;
	char& operator()(int x, int y, int z);
	
	int getBot()const;
	int getTop()const;
	int getBack()const;
	int getFront()const;
	int getLeft()const;
	int getRight()const;
	
	std::string toString()const;
	
	void save(std::string fileName)const;
	
private:
	
	void increaseWidth(int dir);
	void increaseHeight(int dir);
	void increaseDepth(int dir);
	
	/* Increase rate */
	/* One should adjust the value */
	static const unsigned int ir = 5;
	
};

#endif
