
all: cube_of_bits.o square.o quad.o camera.o color3f.o file_chooser.o file_chooser_saver.o main.o math/vec3d.o math/vec3f.o
	g++ -g cube_of_bits.o square.o quad.o camera.o color3f.o file_chooser.o file_chooser_saver.o main.o math/vec3d.o math/vec3f.o -o main -lGL -lGLU -lglut  -lfreeimage `pkg-config gtkmm-3.0 --libs`
	
cube_of_bits.o : cube_of_bits.cpp cube_of_bits.hpp
	g++ -g -c cube_of_bits.cpp -o cube_of_bits.o
	
square.o : square.cpp square.hpp
	g++ -g -c square.cpp -o square.o
	
quad.o : quad.cpp quad.hpp
	g++ -g -c quad.cpp -o quad.o
	
camera.o : camera.cpp camera.hpp
	g++ -g -c camera.cpp -o camera.o
	
color3f.o : color3f.cpp color3f.hpp
	g++ -g -c color3f.cpp -o color3f.o
	
file_chooser.o : file_chooser.cpp file_chooser.hpp
	g++ -g -c `pkg-config gtkmm-3.0 --cflags` file_chooser.cpp -o file_chooser.o
	
file_chooser_saver.o : file_chooser_saver.cpp file_chooser_saver.hpp
	g++ -g -c `pkg-config gtkmm-3.0 --cflags` file_chooser_saver.cpp -o file_chooser_saver.o

main.o : main.cpp
	g++ -g -c `pkg-config gtkmm-3.0 --cflags` main.cpp -o main.o

math/vec3d.o : math/vec3d.cpp math/vec3d.hpp
	g++ -g -c math/vec3d.cpp -o math/vec3d.o
	
math/vec3f.o : math/vec3f.cpp math/vec3f.hpp
	g++ -g -c math/vec3f.cpp -o math/vec3f.o

