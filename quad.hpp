#include "math/vec3d.hpp"
#include "square.hpp"
#include <string>

#ifndef QUAD
#define QUAD

class Quad{

	float vertices[12];
	unsigned int internalColor;
	Square square;
	
public:

	Quad(){}
	Quad(Vec3d position, Vec3d normal, unsigned int internalColor);
	
	unsigned int getInternalColor();
	void setInternalColor(unsigned int internalColor);
	
	Square getSquare()const;
	
	float& operator[](int i);
	const float& operator[](int i)const;
	
	std::string toString()const;

};

#endif
