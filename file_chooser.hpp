// source: http://stackoverflow.com/questions/11483529/show-gtkfilechooserdialog-without-a-parent-window

#include <gtkmm.h>

class FileChooser : public Gtk::FileChooserDialog {
	
public:

    static std::string getFileName();
protected:
    static Gtk::Main kit;
    std::string chosenFile;

    FileChooser(const Glib::ustring& title, Gtk::FileChooserAction action = Gtk::FILE_CHOOSER_ACTION_OPEN) :
    Gtk::FileChooserDialog(title, action) {
        chosenFile = std::string("");
        add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
        add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);
        signal_response().connect(sigc::mem_fun(*this,
                &FileChooser::on_my_response));
    }

    void on_my_response(int response_id);
    
};

