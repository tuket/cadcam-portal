#include "slice.hpp"
#include "vertex.hpp"
#include "edge.hpp"
#include "face.hpp"
#include "cube_of_bits.hpp"
#include <vector>
#include <string>

#ifndef _BREP
#define _BREP

class BREP{
	
	std::vector<Vertex> vertices;
	std::vector<Edge> edges;
	std::vector<Face> faces;
	
	CubeOfBits cob;
	
public:
	
	BREP(const CubeOfBits& cob);
	
	void save(std::string fileName);
	
	
};

#endif
