#include <string>
#include <vector>
#include "cube_of_bits.hpp"

#ifndef SLICE
#define SLICE

class Slice{
	
	char** cell;
	int width, height;
	
public:
	
	Slice(int width, int height);
	Slice(const Slice& slice);
	
	int getWidth()const;
	int getHeight()const;
	
	char& operator()(int x, int y);
	const char& operator()(int x, int y)const;
	
	std::string toString()const;
	
	~Slice();
	
	// static
	
	static std::vector<Slice> getSlicesX(const CubeOfBits& cob);
	static std::vector<Slice> getSlicesY(const CubeOfBits& cob);
	static std::vector<Slice> getSlicesZ(const CubeOfBits& cob);
	
};

#endif
