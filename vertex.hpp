
#ifndef VERTEX
#define VERTEX

class Vertex{
	
public:
	
	Vertex(){}
	Vertex(int x, int y, int z);
	Vertex(const Vertex& vert);
	
	int x, y, z;
	
	bool operator==(const Vertex& vert)const;
	bool operator<(const Vertex& vert)const;
	
};

#endif
